FROM python:3.7-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /src
WORKDIR /src

RUN pip install pipenv
COPY ./Pipfile  /src
COPY ./Pipfile.lock /src
RUN pipenv install --system --skip-lock

COPY . /src
# expose the port 8000
EXPOSE 8000

CMD bash start_django.sh